# Mercury Layouts
This module provides a set of composable layouts intended for, but not exclusive to, the [Mercury Editor](https://www.drupal.org/project/mercury_editor) module.  These layouts are based concepts described in [Every Layout](https://every-layout.dev/), a fantastic learning resource for creating accessible layouts using modern CSS. If you are not familiar with the concepts described in Every Layout, it's highly recommend you take the time to read it to gain a better understanding of some the configuration options available in these layouts.

The included layouts are created using the [Single Directory Components (sdc)](https://www.drupal.org/project/sdc) module which means the underlying components can be used in any Twig template regardless of your theme or module. They are not limited to modules using layout discovery such as Layout Builder or Layout Paragraphs.

## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Included layouts
- Maintainers

## Requirements (required)

This module requires the following modules:

- [Layout Discovery](https://www.drupal.org/project/drupal)
- [Single Directory Components (sdc)](https://www.drupal.org/project/sdc)
- [Style Options](https://www.drupal.org/project/style_options)

## Recommended modules (optional)

- [Layout Paragraphs](https://www.drupal.org/project/layout_paragraphs)
- [Mercury Editor](https://www.drupal.org/project/mercury_editor)

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration (required)

1. Once installed, the provided layouts should appear in the appropriate configuration pages depending on which modules are using layout discovery such as Layout Builder or Layout Paragraphs.


## Included Layouts

- Mercury Stack
- Mercury Two Columns
- Mercury Three Columns (coming soon)
- Mercury Four Columns (coming soon)
- Mercury Cluster


## Maintainers

- John Ferris - [pixelwhip](https://www.drupal.org/u/pixelwhip)